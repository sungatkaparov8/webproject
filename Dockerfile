FROM golang

COPY . /dockeredProject

WORKDIR /dockeredProject

RUN go mod download

ENV GO111MODULE=on\
    CGO_ENABLED=0\
    GOOS=linux\
    GOARCH=amd64
RUN go build /dockeredProject/cmd/web && chmod +x /dockeredProject/cmd/web

ENTRYPOINT ["./web"]

FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /root/

COPY --from=0 /dockeredProject .

CMD ["./web"]

EXPOSE 4000